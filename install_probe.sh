#!/bin/bash

# Based on http://www.satsignal.eu/raspberry-pi/dump1090.html

function error_then_quit {
    if [ $1 != 0 ]; then
      printf "\n#--------------------------------------------------------------------#\n"
      printf "   There was an error during installation. Please proceed manually...   "
      printf "\n#--------------------------------------------------------------------#\n"
      exit $1
    fi
}


## Update Aptitude
printf "\n\n... Update Aptitude's packages\n\n"
sudo apt-get update && sudo apt-get upgrade
error_then_quit $?

printf "\n\n... Install required packages\n\n"
sudo apt-get install cmake libusb-1.0-0-dev build-essential --yes
error_then_quit $?

printf "\n\n... Create sources directory\n\n"
cd ~/sources
error_then_quit $?

printf "\n\n... Clone RTL-SDR repository\n\n"
git clone git://git.osmocom.org/rtl-sdr.git
error_then_quit $?
cd rtl-sdr

printf "\n\n... Prepare build process\n\n"
mkdir build
error_then_quit $?
cd build

printf "\n\n... Create the make file\n\n"
cmake ../ -DINSTALL_UDEV_RULES=ON
error_then_quit $?

printf "\n\n... Launch the MAKE\n\n"
make
error_then_quit $?

printf "\n\n... Proceed to the installation\n\n"
sudo make install
error_then_quit $?

printf "\n\n... Create the blacklist file\n\n"
sudo echo "blacklist dvb_usb_rtl28xxu" > /etc/modprobe.d/ban_rtl.conf
error_then_quit $?

printf "\n\n... Launch LDCONFG\n\n"
sudo ldconfig
error_then_quit $?


printf "\n\n... Clone Dump1090\n\n"
cd ~/sources
git clone git://github.com/MalcolmRobb/dump1090.git
error_then_quit $?

printf "\n\n... Build the binary\n\n"
cd dump1090
make
error_then_quit $?



printf "\n\n --------------------- ALRIGHT! EVERYTHING IS DONE, YOU NEED TO TEST !  ---------------------\n\n"
printf "\n   1. Reboot your machine"
printt "\n   2. Launch rtl_test -t : if you have errors, debug yourself !"
printf "\n   3. Launch ~/sources/dump1090/dump1090 --net --interactive : you should see planes !"
