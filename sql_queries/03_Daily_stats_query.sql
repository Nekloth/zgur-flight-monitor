#Statistiques sur les avions d'HIER
#   Date: 2016.11.23
#   Commentaires: Contient les stats suivantes:
#					- Date de stats
#                   - Nombre total d'avions
#					- Nombre d'avion qui sont passés à plus de 8000m
#					- Nombre d'avion qui sont passés à moins de 8000m
#					- Altitude moyenne des avions-hauts
#					- Altitude moyenne des avions-bas
#					- Altitude la plus basse de la journée
#					- Première heure de passage (bas only)
#					- Dernière heure de passage (bas only)


SET @limit_altitude=8000;
SET @day_diff=0;


SELECT 
	DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY)) as stats_date,
	COUNT(*) as number_of_planes,
    (
		SELECT COUNT(*)
		FROM aerostuff.flights_villecresnes
		WHERE 1=1
			AND DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY))
            AND altitude >= @limit_altitude
    ) as number_high,
    (
		SELECT COUNT(*)
		FROM aerostuff.flights_villecresnes
		WHERE 1=1
			AND DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY))
            AND altitude < @limit_altitude
    ) as number_low,    
	ROUND((
		SELECT AVG(altitude)
		FROM aerostuff.flights_villecresnes
		WHERE 1=1
			AND DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY))
            AND altitude >= @limit_altitude
    )) as avg_altitude_high,
	ROUND(AVG(altitude)) as avg_altitude_low,
    ROUND(MIN(altitude)) as lowest_altitude,
	DATE_FORMAT( MIN(FROM_UNIXTIME(LEFT(seen_at,10))),"%H:%i") as seen_at_start,
    DATE_FORMAT( MAX(FROM_UNIXTIME(LEFT(seen_at,10))),"%H:%i") as seen_at_end
FROM aerostuff.flights_villecresnes
WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY))
;
